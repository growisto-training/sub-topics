//global execution context

//es5
//This is know TDZ - temporal dead zone
//This works on scope chain


// let var1 = 10
// let var2 = 30;

// //scope1
// function myFunc() {
//     //function execution context
//     let funvar = 10;

//     //scope 2
//     {
//         var funvar2 = 30;
//     }
// }

// console.log('funvar2', funvar2);

// console.log(var1);

//es6
// let var1 = 10;
// let var2 = 10;

// // console.log(THIS_IS_MY_CONST_VAR);
// const THIS_IS_MY_CONST_VAR = 10;
// //this are initialized only once

// //TEMPLATE STRINGS

// //es5 
// var fname = 'surya';
// var lname = 'batchu';

// var str = 'My name is'+fname + ' '+lname+'';
// console.log(str);

// //concanete
// //addition

// let tempStr = `My name is ${fname} ${lname}. 'Hello' "hey"`;

// console.log('str', tempStr);


//ENHANCED LITERAL OBJECTS

//PRIMITIVE DATATYPE = string, number, boolean, null, undefined
//COmplex datatypes = Object;

// var var1 = 100;
// var var2 = 200;
// var var3 = 10;
// var var4 = 10;

//literal
//array = []
//Obj = {}
//variables in object are called as properties
//ES5
// var obj = {
//     var1: var1,
//     var2: var1,
//     var5: 30
// }


// //ENHANCED LITERAL OBJECTS
// //ES6
// const myProp = `myProp${var4}`;

// var obj = {
//     var1,
//     var2,
//     var5: 30,
//     'Surya Batchu': 'string',
//     [`myProp${var4}`]: 'custom', //complex property
// }

// // console.log('obj', obj);

// function getProps(fname, lname) {
//     return { fname, lname }
// }

// console.log(getProps('First name', 'last name'))

//Destructuring

//objects
// const {var1:newvar1, var2:newvar2, surya} = obj;

//arrays
// const [a1, a2, a3] = [10, 20];

// //0 index = 10
// //1 index = 20

// const {0: myvar1, 1: myvar2} = [100, 200, 300];
// //0 index = 100
// //1 index = 200

// console.log(newvar1, var2, surya);

// //this will give me error
// let normal = 30;//this is cz of tdz
// var normal = 20;

// console.log(a1, a2, a3);

//arrays 
//associative array => this is your object, when you name the variables
//indexed array = this is an object

//default params
//default params only work if the param values are undefined.
// function myFunc(param = 100) {
//   console.log("This is my param", param)
// }

// var d = undefined;

// // myFunc(20) // this outputs the param1 to 20
// myFunc(false) //this outputs the param1 to 10 if no parameter value is present


//rest 
// let arr = [10, 20];

// let [item1, item2, ...preethamArray] = arr;

// var {prop1, ...preethamObj1} = {
//     prop1: 10,
//     // prop2: 30,
//     prop2: 90,
// }

// console.log(preethamObj1);
// let obj1 = {};

// let [item1, item2, ...restVars] = arr;


// console.log(item1, preethamArray);

//item1 will have value as 10

//restVars will have an array of rest variables

//spread

// let arr = [1,2,3];
// let arr2 = [4,5];
  
// arr = [...arr,...arr2];
// console.log(arr); 

// var arr = [10, 20, 30];
// var pushArray = [40, 50, 60, 70, 8, 90];

// arr.push(100);

// arr.push(...pushArray);

// console.log(arr);

// [10, 20, 30, 100, 40, 50];


//JSON
const data = require('./data.json');
// console.log(data);


//this pointer does not have the function scope
//this pointer will have the parent scope

//function expression
const functionExpression = function(x) {
    console.log(this);
    return x * 2 ;
}


//GEX

//short hand property for arrow function

//return value in single line statement
const multiplyBy2 = (x) => x * 2;

const normalFunc  = () => {
    //arrow function
    const multiplyBy2 = (x) => {
        console.log(this);
        return  x * 2;
    };
    multiplyBy2(2);
}

//this pointer
//in strict mode the this pointer will be as undefined

//custom prototype
function MyFunctionConstructorPerson(name, age) {
    // return {name, age}
    this.name = name;
    this.age = age;
    this.display = () =>
        `Full name is ${this.name} and age is ${this.age}`

    // console.log("this", this);
}

//whenever you create a function contstuctor, it contains of this keyowr
var obj = {};
var obj1 = new Object();

// var arr = [];
// var arr1 = new Array();

// var myObj = new MyFunctionConstructorPerson("surya", 22);

// var myObj2 = new MyFunctionConstructorPerson("preetham", 22);

// var myObj3 = new MyFunctionConstructorPerson("athresh", 22);

// console.log(myObj.display());

// console.log("myObj", myObj)

//new is also a operator which binds
//a this pointer to the object

// es6
// import {getAddition} from './module1.js';
// import getAddition from './module1.js';

// import statement should always be at the top

//es5

// const module1 = require('./module1.js');
// const {getAddition} = require('./module1.js');


// console.log('data', getAddition(2, 2))

//es5 
// function MyFunctionConstructorPerson(name, age) {
//     // return {name, age}
//     this.name = name;
//     this.age = age;
//     this.display = () =>
//         `Full name is ${this.name} and age is ${this.age}`

//     // console.log("this", this);
// }

//es6 way of function constructor

//inheritance
//this is a type multilevel inheritance(single inheritance)

class Parent {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }

    //es6
    // display = () => `Full name is ${this.name} and age is ${this.age}`;
   
    //es5
    display() { return `Full name is ${this.name} and age is ${this.age}`};
}

class Child extends Parent {
    display() { return `Im child and the name is ${this.name} and age is ${this.age}`};

    childDisplay() {
        return `my parent name is ${this.name}`
    }
}

class Rectangle {
    constructor(w, h) {
        this.w = w;
        this.h = h;
    }
}

Rectangle.prototype.area = function() {
    return this.w * this.h
}

class Square extends Rectangle{
    constructor(side){
        super(side, side)
        this.area = super.area
    }
}

const classObj = new Child("Shailesh", 25);

console.log(classObj.display());

console.log(classObj.childDisplay());