<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="">
</head>

<body>
  <img src="" id="buddy" alt="" width="480">
  <script>

    // With Async await
    Promise.all([
      displayDogs('german-shefard.jpg'),
      displayDogs('labrador.jpeg'),
      displayDogs('golden.jpeg')
    ]).catch(err => {
      console.log(err)
    })

    async function displayDogs(src) {
      const response = await fetch(src)
      const blob = await response.blob()
      const img = document.createElement('img')
      img.src = URL.createObjectURL(blob)
      img.style.width = '480'
      document.body.appendChild(img)
    }

    // With Promise
    // console.log('about to fetch an Image');
    // fetch('german-shefard.jpg')
    //   .then(response => {
    //     console.log(response);
    //     return response.blob()
    //   }).then(blob => {
    //     console.log(blob);
    //     document.getElementById('buddy').src = URL.createObjectURL(blob)
    //   })
    // .catch(err => {
    //     console.log(err);
    //   })
  </script>
</body>

</html>