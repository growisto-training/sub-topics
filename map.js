
// const myNewArray = myArr.map((elem, index, self) => {
//     console.log('elem', elem);
//     console.log('index', index);
//     console.log('self', self);

//     return 2 * elem;
// })

// console.log("myNewArray",myNewArray)

// function myMap(myArr, callback) {
//     for(var index in myArr) {
//         if(typeof callback === 'function') {
//             callback(myArr[index], index, myArr);
//         }
//     }
// };

Array.prototype.myMap = function myMap(callback) {
    for(let index = 0; index < this.length; index++) 
        if(typeof callback === 'function') callback(this[index], index, this);
}

const myArr = [10, 20];
// };(myArr, callback) {
//     for(var index in myArr) {
//         if(typeof callback === 'function') {
//             callback(myArr[index], index, myArr);
//         }
//     }
// };

// myArr.myMap(() => 
myArr.myMap((elem, index, self) => {
    console.log('elem', elem);
    console.log('index', index);
    console.log('self', self);
});

//3 types of loop
//for native loop

//works for both array and object
//for in => this access property name
//for of => this access value of object or array