// Iterators
function makeRangeIterator (start = 0, end = Infinity, step = 1) {
  let nextIndex = start
  let iterationCount = 0
  const rangeIterator = {
    next: function () {
      let result
      if (nextIndex < end) {
        result = { value: nextIndex, done: false }
        nextIndex += step
        iterationCount++
        return result
      }
      return { value: iterationCount, done: true }
    },
    previous: function () {
      let result
      if (nextIndex > end) {
        result = { value: nextIndex, done: false }
        nextIndex -= step
        iterationCount++
        return result
      }
      return { value: iterationCount, done: true }
    }
  }
  return rangeIterator
}

// Generators
function * makeRangeIteratorGen (start = 0, end = 100, step = 5) {
  let iterationCount = 0
  for (let i = 0; i < end; i += step) {
    iterationCount++
    yield i
  }
  return iterationCount
}
console.log(makeRangeIteratorGen(1, 10, 2))

const sequence = makeRangeIteratorGen(1, 10, 2)
console.log(sequence.next().value)
console.log(sequence.next().value)


// Modified Iteraror Method
function makeRangeIteratorModified (start = 0, end = Infinity, step = 1) {
  let nextIndex = start
  let iterationCount = 0
  return () => {
    let result
    if (nextIndex < end) {
      result = { value: nextIndex, done: false }
      nextIndex += step
      iterationCount++
      return result
    }
    return { value: iterationCount, done: true }
  }
}

const it = makeRangeIterator(10, 1, 2)

let result = it.previous()
// console.log(result)
while (!result.done) {
  console.log(result.value)
  result = it.previous()
  console.log(result.done)
}

console.log('Iterated over sequence of size: ', result.value)

function* fibonacci() {
  let current = 0;
  let next = 1;
  while (true) {
    let reset = yield current;
    [current, next] = [next, next + current];
    if (reset) {
        current = 0;
        next = 1;
    }
  }
}

// const sequence = fibonacci()

